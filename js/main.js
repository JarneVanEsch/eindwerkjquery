$(document).ready(function() {
    // mustafa
    // fullPage.js instellingen
    $('#fullpage').fullpage({
        css3: true,
        
        //koen
        onLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex){
            //leaving the first slide of the 2nd Section to the right
            $('#svg').fadeIn('fast');
            var mySVG = $('#svg').drawsvg();
            mySVG.drawsvg('animate');
        },
        afterRender: function(){
           var inst = $('[data-remodal-id=modal]').remodal();
           inst.open();
           $(document).on('opened', '#remodal', function () {
         });
           $('#remodal, .remodal-wrapper').css('display','block').animate({opacity:1},3500);
           
       }
    });  
   
    $("a#red, a#blue")
        .on('mouseenter', function showTooltip(){
            $('<div class="tooltip">Verander van kleur</div>').appendTo('body');
        })
        .on('mouseleave', function hideTooltip(){
            $('div.tooltip').remove();
        })
        .on('mousemove', function ChangeTooltipPos(event){
            var tooltipX = event.pageX - 8;
            var tooltipY = event.pageY + 18;
            $('div.tooltip').css({top: tooltipY, left: tooltipX});
        });
        
        $("section.lazy").show().lazyload({
            effect: "fadeIn"
        });
    
    // jarne
    // Blinking arrows
    var $element1 = $("#mogelijkheden > div.fp-controlArrow.fp-prev");
            var $element2 = $("#mogelijkheden > div.fp-controlArrow.fp-next");
            $($element1).mouseenter(function(){
                $($element1).fadeOut(800).fadeIn(800).fadeOut(800).fadeIn(800).fadeIn(800).fadeOut(800).fadeIn(800).fadeIn(800).fadeOut(800).fadeIn(800);
            }) 
            
            $($element1).mouseleave(function(){
                $($element1).fadeIn(800);
            }) 
            
            $($element2).mouseenter(function(){
                $($element2).fadeOut(800).fadeIn(800).fadeOut(800).fadeIn(800).fadeIn(800).fadeOut(800).fadeIn(800).fadeIn(800).fadeOut(800).fadeIn(800);
            }) 
            
            $($element2).mouseleave(function(){
                $($element2).fadeIn(800);
            }) 

     var $link = $('.css');
            $('.cssbtn').on('click', function(){
                var $stylesheet = $(this).text().toLowerCase();
                $link.attr('href','css/' + $stylesheet + '.css');
                $('.cssbtn').removeClass('active');
                $(this).addClass('active');
            });

    // mustafa
    // ajax inlezen
    function getContent(filename) {
          $.ajax({
            url: filename,
            type: 'GET',
            dataType: 'html',
            beforeSend: function() {
              $('#ajax').html('<img id="loader" src="img/loading.gif">');
            },
            success: function(data, textStatus, xhr){
              $('#ajax').html(data).hide().slideDown("slow");
            },
            error: function(xhr, textstatus, errorThrown){
              $('#ajax').html(textstatus);

            }
          });
        }
        $('#html').on('click', function() {
            getContent('data/html.html');
            $('#vakken ul li a').removeClass('active');
            $(this).addClass('active');
        });
        $('#interface').on('click', function() {
            getContent('data/interface.html');
            $('#vakken ul li a').removeClass('active');
            $(this).addClass('active');
        });
        $('#photoshop').on('click', function() {
            getContent('data/photoshop.html');
            $('#vakken ul li a').removeClass('active');
            $(this).addClass('active');
        });
        $('#drupal').on('click', function() {
            getContent('data/drupal.html');
            $('#vakken ul li a').removeClass('active');
            $(this).addClass('active');
        });
        $('#php').on('click', function() {
            getContent('data/php.html');
            $('#vakken ul li a').removeClass('active');
            $(this).addClass('active');
        });
        $('#webanimatie').on('click', function() {
            getContent('data/webanimatie.html');
            $('#vakken ul li a').removeClass('active');
            $(this).addClass('active');
        });
        $('#jquery').on('click', function() {
            getContent('data/jquery.html');
            $('#vakken ul li a').removeClass('active');
            $(this).addClass('active');
        });
        $('#magento').on('click', function() {
            getContent('data/magento.html');
            $('#vakken ul li a').removeClass('active');
            $(this).addClass('active');
        });
        $('#frameworks').on('click', function() {
            getContent('data/frameworks.html');
            $('#vakken ul li a').removeClass('active');
            $(this).addClass('active');
        });
        $('#workflows').on('click', function() {
            getContent('data/workflows.html');
            $('#vakken ul li a').removeClass('active');
            $(this).addClass('active');
        });
        $('#training').on('click', function() {
            getContent('data/training.html');
            $('#vakken ul li a').removeClass('active');
            $(this).addClass('active');
        });
        $('#eindwerk').on('click', function() {
            getContent('data/eindwerk.html');
            $('#vakken ul li a').removeClass('active');
            $(this).addClass('active');
        });       
});